﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Reflection;

namespace SqliteExample
{
    class Program
    {
        static void Main(string[] args)
        {
            //Make sure the database is there if it isn't then make one. 
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "TestDB.sqlite");
            if (!File.Exists(path))
                //This is the create command that will make a sqlite database you can also creat a .db. 
                SQLiteConnection.CreateFile("TestDB.sqlite");
            
            // We need our connection string. 
            string dbConnection = "Data Source=TestDB.sqlite; Version=3;";

            ///I like using the using when anything is opened. It is supposed to help with garbage collection. 
            ///We will use the using to open up a new sqlite connection
            using (SQLiteConnection conn = new SQLiteConnection(dbConnection))
            {
                //Open the Connection. 
                conn.Open();

                //*******************Typically this is the format to do a query with no return.**********************
                //Lets create a table for the highscores.
                string tableCreating = "CREATE TABLE highscores (name VARCHAR(20), score INT)";
                using (SQLiteCommand cmd = new SQLiteCommand(tableCreating, conn))
                {
                    cmd.ExecuteNonQuery();
                }

                //Lets fill in the table. 
                List<string> TableEntries = new List<string>() { "INSERT INTO highscores (name, score) VALUES ('Test1', 10)", "INSERT INTO highscores (name, score) VALUES ('Test2', 50)", "INSERT INTO highscores (name, score) VALUES ('Test3', 37)" };
                foreach (var entry in TableEntries)
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(entry, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }

                //*******************Typically this is the format to do a query with return.**********************
                string tableQuery = "SELECT * FROM highscores ORDER BY score DESC";
                using (SQLiteCommand cmd = new SQLiteCommand(tableQuery, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Console.WriteLine("Name: {0}\tScore {1}: ", rdr["name"], rdr["score"]);
                        }
                    }
                }
            }
        }
    }
}
